using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Builder : MonoBehaviour
{
    [SerializeField] private List<BuildTamplate> _tamplates;

    [SerializeField] private BuildTamplate _selectedTamplate;

    public void Init()
    {
        foreach (var tamplate in _tamplates)
            tamplate.OnSelected += SetSelected;
    }

    private void SetSelected(BuildTamplate tamplate)
    {
        if (tamplate == _selectedTamplate)
            _selectedTamplate = null;
        else
            _selectedTamplate = tamplate;
    }

    public void Build(Cell cell)
    {
        if (!_selectedTamplate || !cell)
            return;

        Instantiate(_selectedTamplate.Prefab, cell.Position, Quaternion.identity);
        _selectedTamplate = null;
    }
}
