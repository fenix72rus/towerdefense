using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BuildTamplate : MonoBehaviour, IClickable
{
    public event Action<BuildTamplate> OnSelected;

    public Transform Transform => this.transform;
    public Entity Prefab => _prefab;

    [SerializeField] private Entity _prefab;

    public void Select()
    {
        OnSelected?.Invoke(this);
    }

    public void ProcessClick()
    {
        Select();
    }
}
