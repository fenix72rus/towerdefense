using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClickable 
{
    Transform Transform { get; }

    void ProcessClick();
}
