using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BuildHandler : ClickHandler
{
    public event Action<Cell> OnCellClicked;

    public BuildHandler(IClickStateSwitcher stateSwitcher) : base(stateSwitcher)
    {
    }

    public override void Proccess(IClickable clickable)
    {
        if (clickable.Transform.TryGetComponent(out BuildTamplate tamplate))
        {
            tamplate.Select();
        }
        if (clickable.Transform.TryGetComponent(out Cell cell))
        {
            OnCellClicked?.Invoke(cell);
            _stateSwitcher.SwitchState<IdleHandler>();
        }
    }

    public override void Start()
    {
    }

    public override void Stop()
    {
    }
}
