using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleHandler : ClickHandler
{
    public IdleHandler(IClickStateSwitcher stateSwitcher) : base(stateSwitcher)
    {
    }

    public override void Proccess(IClickable clickable)
    {
        clickable.ProcessClick();

        if(clickable.Transform.TryGetComponent(out IInteractable interactable))
        {
            interactable.Interact();
        }
        if(clickable.Transform.TryGetComponent(out BuildTamplate tamplate))
        {
            _stateSwitcher.SwitchState<BuildHandler>();
        }
    }

    public override void Start()
    {
    }

    public override void Stop()
    {
    }
}
