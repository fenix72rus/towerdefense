﻿using UnityEditor;
using UnityEngine;

public abstract class ClickHandler
{
    protected readonly IClickStateSwitcher _stateSwitcher;

    protected ClickHandler(IClickStateSwitcher stateSwitcher)
    {
        _stateSwitcher = stateSwitcher;
    }

    public abstract void Proccess(IClickable clickable);

    public abstract void Start();

    public abstract void Stop();
}