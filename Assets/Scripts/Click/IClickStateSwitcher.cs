public interface IClickStateSwitcher
{
    void SwitchState<T>() where T : ClickHandler;
}
