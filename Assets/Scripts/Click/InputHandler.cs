using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputHandler : MonoBehaviour
{
    public event Action<IClickable> OnClicked;

    [SerializeField] private Camera _mainCamera;

    private void OnValidate()
    {
        if (!_mainCamera)
            _mainCamera = Camera.main;
    }

    private void Update()
    {
        MouseInput();
    }

    private void MouseInput()
    {
        if (Input.GetMouseButtonDown(0))
            TakeTouch(Input.mousePosition);
    }

    private void TakeTouch(Vector3 touchPosition, int id = -1)
    {
        Ray ray = _mainCamera.ScreenPointToRay(touchPosition);

        var hits = Physics2D.RaycastAll(ray.origin, ray.direction, 100f);

        if (hits == null)
            return;

        for(int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.TryGetComponent(out IClickable clickable))
            {
                OnClicked?.Invoke(clickable);
            }
        }
    }
}
