using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ClickController : IClickStateSwitcher
{
    private ClickHandler _currentState;
    private List<ClickHandler> _allStates;

    public ClickController(Builder builder)
    {
        _allStates = new List<ClickHandler>();
        _allStates.Add(new IdleHandler(this));
        var buildHandler = new BuildHandler(this);
        buildHandler.OnCellClicked += builder.Build;
        _allStates.Add(buildHandler);
        _currentState = _allStates[0];
    }

    public void Process(IClickable clickable)
    {
        _currentState.Proccess(clickable);
    }

    public void SwitchState<T>() where T : ClickHandler
    {
        var state = _allStates.FirstOrDefault(s => s is T);
        _currentState.Stop();
        state.Start();
        _currentState = state;
    }
}
