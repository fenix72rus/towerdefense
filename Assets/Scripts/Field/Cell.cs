using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour, IClickable
{
    public Transform Transform 
    {
        get
        {
            if (!_transform)
                _transform = this.transform;
            return _transform;
        }
    }
    public Vector3 Position { get => Transform.position; }

    private Transform _transform;

    public void ProcessClick()
    {
    }
}
