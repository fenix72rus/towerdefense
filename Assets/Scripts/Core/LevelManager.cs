using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private Builder _builder;
    [SerializeField] private InputHandler _inputHandler;

    private ClickController _clickController;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        _builder.Init();
        _clickController = new ClickController(_builder);
        _inputHandler.OnClicked += _clickController.Process;
    }
}
